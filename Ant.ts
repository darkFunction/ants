/// <reference path="Actor.ts" />
/// <reference path="World.ts" />

enum AntBehaviour { Foraging, Returning };
enum PheromoneType { Nest, Food };

class Ant implements Actor {

	// Random angle adjustment every frame if not following a scent.
	private static WIGGLE = 0.3;

	private static SPEED = 0.5;
	private static TURN_SPEED = 0.2;

	// Ants drop 1.0 pheromone scent minus a decay for each frame they are away from 
	// the nest.
	private static PHEROMONE_DECAY = 0.001;

	// How far each drop of scent spreads, in pixels.
	private static PHEROMONE_SPREAD = 3;

	// How quickly the pheromone evaporates.
	public static PHEROMONE_FADE = 0.0003;

	private angle:number;
	private speed:number;
	private behaviour:AntBehaviour;
	private pheromoneStrength:number;

	size:number = 2;

	constructor(public pos:Vec, public world:World) {
		this.angle = Math.random() * Math.PI * 2;
		this.behaviour = AntBehaviour.Foraging; 
		this.pheromoneStrength = 1.0;
	}

	update() {
		(this.behaviour == AntBehaviour.Foraging) ? this.checkFood() : this.checkNest();

		var scent:number = this.sniff();
		if (Math.abs(scent) > 0.01) {
			this.angle += (scent > 0) ? Ant.TURN_SPEED : -Ant.TURN_SPEED;
		} else {
			this.angle += ((Math.random() - 0.5) * 2) * Ant.WIGGLE; 
		}
		
		this.move();
		this.dropPheromone();		
	}

	private checkNest() {
		var nest:Actor = this.world.nest;
		if (this.pos.insideCircle(nest.pos.x, nest.pos.y, nest.size/2)) {
			this.pheromoneStrength = 1.0;	
			this.behaviour = AntBehaviour.Foraging;
		}
	}	

	private checkFood() {
		var food:Food = this.world.foodAtPos(this.pos);
		if (food) {
			// Found food!
			food.size -= 0.5;
			this.behaviour = AntBehaviour.Returning;
			this.pheromoneStrength = 1.0;

			// Turn 180
			this.angle += Math.PI; 
		}
	}

	private move() {
		var deltaX = Math.cos(this.angle) * Ant.SPEED;
		var deltaY = Math.sin(this.angle) * Ant.SPEED;
		if (this.pos.x + deltaX < 0 || this.pos.x + deltaX > this.world.width) {
			deltaX = 0;
		}
		if (this.pos.y + deltaY < 0 || this.pos.y + deltaY > this.world.height) {
			deltaY = 0;
		}
		this.pos.x += deltaX;
		this.pos.y += deltaY;
	}
	
	private sniff() {
		var leftAntennaPos:Vec = this.pos.displaced(this.angle + Math.PI/4, 3); 
		var rightAntennaPos:Vec = this.pos.displaced(this.angle - Math.PI/4, 3); 

		var pheromoneType:PheromoneType = this.behaviour == AntBehaviour.Foraging ? PheromoneType.Food : PheromoneType.Nest;

		var leftScent:number = this.world.getPheromoneAt(leftAntennaPos, pheromoneType);
		var rightScent:number = this.world.getPheromoneAt(rightAntennaPos, pheromoneType);

		return leftScent - rightScent;
	}

	private dropPheromone() {
		if (this.pheromoneStrength > 0) {
			var pheromoneType:PheromoneType = this.behaviour == AntBehaviour.Foraging ? PheromoneType.Nest : PheromoneType.Food;
			this.world.dropPheromone(pheromoneType, this.pos, this.pheromoneStrength, Ant.PHEROMONE_SPREAD);
			this.pheromoneStrength -= Ant.PHEROMONE_DECAY;
		}
	}

}
