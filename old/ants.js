(function (window, canvas) {
    'use strict';
    var Renderer = function (canvas) {
        var context = canvas.getContext("2d");
        var drawDots = function (dots, colour, size) {
            context.fillStyle = colour;
            for (var i = 0; i < dots.length; ++i) {
                var dot = dots[i];
                context.fillRect(dot.x - (size / 2), dot.y - (size / 2), size, size);
            }
        };
        var drawGround = function () {
            context.fillStyle = '#00BB00';
            context.fillRect(0, 0, canvas.width, canvas.height);
        };
        this.render = function (world) {
            context.clearRect(0, 0, canvas.width, canvas.height);
            drawGround();
            context.fillStyle = '#BBBB00';
            for (var x = 0; x < world.pheromones.length; ++x) {
                for (var y = 0; y < world.pheromones[x].length; ++y) {
                    if (world.pheromones[x][y] != -1) {
                        context.fillRect(x, y, 1, 1);
                    }
                }
            }
            drawDots(world.ants, "#000000", 1);
            drawDots(world.food, "#FF0000", 3);
        };
    };
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
    function forEach(array, task, args) {
        for (var i = 0; i < array.length; ++i) {
            array[i][task](args);
        }
    }
    function new2DArray(w, h, value) {
        var array = [w];
        for (var col = 0; col < w; ++col) {
            array[col] = [h];
            for (var row = 0; row < h; ++row) {
                array[col][row] = value;
            }
        }
        return array;
    }
    var Ant = function (x, y) {
        var Mode = { FORAGE: 0, CARRY: 1 };
        var Dir = { LEFT: 0, RIGHT: 1, UP: 2, DOWN: 3 };
        this.x = x;
        this.y = y;
        this.mode = Mode.FORAGE;
        this.distance = 0;
        this.lastMove = null;
        var directions = { "left": [-1, 0],
            "right": [+1, 0],
            "up": [0, -1],
            "down": [0, +1] };
        this.update = function (pheromones) {
            var tileDistance = pheromones[this.x][this.y];
            if (tileDistance > this.distance || tileDistance == -1) {
                pheromones[this.x][this.y] = this.distance;
            }
            var neighbours = [directions.left, directions.right, directions.up, directions.down];
            if (this.lastMove) {
                var index = neighbours.indexOf(this.lastMove);
                neighbours.splice(index, 1);
            }
            var getNeighbourDistance = function (pheromones, x, y, distance, xMod, yMod) {
                var d = pheromones[x + xMod][y + yMod];
                if (d == -1 || d > distance + 1) {
                    d = distance + 1;
                }
                return d;
            };
            var min, max = -1;
            var scores = [neighbours.length];
            for (var i = 0; i < neighbours.length; ++i) {
                var neighbour = neighbours[i];
                var distance = getNeighbourDistance(pheromones, this.x, this.y, this.distance, neighbour[0], neighbour[1]);
                if (i == 0) {
                    min = distance;
                    max = distance;
                }
                else {
                    if (min > distance) {
                        min = distance;
                    }
                    if (max < distance) {
                        max = distance;
                    }
                }
                scores[i] = distance;
            }
            for (var i = 0; i < scores.length; ++i) {
                scores[i] -= (min - 1);
                if (i > 0) {
                    scores[i] += scores[i - 1];
                }
            }
            var r = getRandomInt(0, scores[scores.length - 1]);
            for (var i = 0; i < scores.length; ++i) {
                if (r < scores[i]) {
                    r = i;
                    break;
                }
            }
            this.lastMove = neighbours[r];
            this.x = this.x + neighbours[r][0];
            this.y = this.y + neighbours[r][1];
            this.distance++;
            tileDistance = pheromones[this.x][this.y];
            if (tileDistance != -1 && this.distance > tileDistance) {
                this.distance = tileDistance;
            }
        };
    };
    var Food = function () {
        this.x = getRandomInt(0, canvas.width);
        this.y = getRandomInt(0, canvas.height);
    };
    var World = function (size, numAnts, numFood) {
        this.food = [];
        this.ants = [];
        this.pheromones = new2DArray(size.w, size.h, -1);
        for (var i = 0; i < numAnts; ++i) {
            this.ants.push(new Ant(size.w / 2, size.h / 2));
        }
        for (var i = 0; i < numFood; ++i) {
            this.food.push(new Food());
        }
        this.update = function () {
            forEach(this.ants, "update", this.pheromones);
        };
    };
    var renderer = new Renderer(canvas);
    var world = new World({ w: canvas.width, h: canvas.height }, 100, 100);
    var tick = function () {
        world.update();
        renderer.render(world);
    };
    window.setInterval(tick, 1);
}(this, document.getElementById("simulation")));
