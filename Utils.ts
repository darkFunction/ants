function initialise2DArray(w, h, value):Array<Array<number>> {
	var array = [w]
	for (var col=0; col<w; ++col) {
		array[col] = [h]
		for (var row=0; row<h; ++row) {
			array[col][row] = value;
		} 
	}
	return array;
}

function sq(n):number {
	return n*n;
}


