/// <reference path="Utils.ts" />

class Vec {
	constructor(public x:number, public y:number) {}
	
	insideCircle(cx:number, cy:number, radius):boolean {
		return sq(this.x - cx) + sq(this.y - cy) < sq(radius);
	}
		
	displaced(angle:number, distance:number):Vec {
		return new Vec(
			this.x + (distance * Math.cos(angle)),
			this.y + (distance * Math.sin(angle))
		);
	}
}


