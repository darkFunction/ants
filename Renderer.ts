/// <reference path="Actor.ts" />

class Colour {
	constructor(public r:number, public g:number, public b:number, public a:number) {}
}

class Renderer {
	private context:CanvasRenderingContext2D;
	private width:number;
	private height:number;
	private imageData:ImageData;
	private backContext:any;

	constructor(canvas:any) {
		this.context = canvas.getContext("2d");
		this.width = canvas.width;
		this.height = canvas.height;

		var backCanvas = document.createElement('canvas');
		backCanvas.width = this.width;
		backCanvas.height = this.height;
		this.backContext = backCanvas.getContext('2d');
		this.imageData = this.context.createImageData(canvas.width, canvas.height);
	}

	render(ants:Array<Actor>, food:Array<Actor>, nest, nestPheromone:Array<Array<number>>, foodPheromone:Array<Array<number>>) {
		this.context.clearRect(0, 0, this.width, this.height);
		this.drawGround();	
		this.drawPheromones(nestPheromone, new Colour(0, 255, 0, 255));
		this.drawPheromones(foodPheromone, new Colour(255, 255, 0, 255));
		this.drawNest(nest);
		this.drawActors(ants, '#000000');
		this.drawActors(food, '#FF0000');
	}

	private drawNest(nest:Actor) {
		this.context.fillStyle = '#8F6911';
		this.drawActor(nest);
	}

	private drawGround() {
		this.context.fillStyle = '#00BB00';
		this.context.fillRect(0, 0, this.width, this.height);
	}

	private drawPheromones(pheromones:Array<Array<number>>, colour:Colour) {
		this.context.fillStyle = '#00FF00';
		var pos:Vec = new Vec(0, 0);
		for (pos.x=0; pos.x<pheromones.length; ++pos.x) {
			for (pos.y=0; pos.y<pheromones[pos.x].length; ++pos.y) {
				colour.a = 255 * pheromones[pos.x][pos.y]
				this.setPixel(this.imageData, pos, colour);
			}
		}
		this.backContext.putImageData(this.imageData, 0, 0);
		this.context.drawImage(this.backContext.canvas, 0, 0);
	}

	private drawActors(actors:Array<Actor>, colour) {
		this.context.fillStyle = colour;
		for (var i=0; i<actors.length; ++i) {
			this.drawActor(actors[i]);
		}	
	}

	private drawActor(actor:Actor) {
		var radius = actor.size/2;
		this.context.beginPath();
		this.context.arc(actor.pos.x, actor.pos.y, radius, 0, 2*Math.PI);
		this.context.fill();
	}

    private setPixel(imageData:ImageData, pos:Vec, colour:Colour) {
    	var i = (pos.x + pos.y * imageData.width) * 4;
		imageData.data[i] = colour.r;
		imageData.data[i + 1] = colour.g;
		imageData.data[i + 2] = colour.b;
		imageData.data[i + 3] = colour.a;
	}

}


