/// <reference path="Actor.ts" />
/// <reference path="Ant.ts" />
/// <reference path="Food.ts" />
/// <reference path="Nest.ts" />
/// <reference path="Utils.ts" />

class Pheromones {
	nest:Array<Array<number>>;
	food:Array<Array<number>>;
	constructor(width:number, height:number) {
		this.nest = initialise2DArray(width, height, 0);
		this.food = initialise2DArray(width, height, 0);
	}
}

class World {
	ants:Array<Actor>;
	food:Array<Actor>;	
	nest:Actor;
	pheromones:Pheromones;
	
	constructor(public width:number, public height:number, numAnts:number, numFood:number) {

		this.ants = new Array<Actor>();
		for (var i:number=0; i<numAnts; i++) {
			this.addAnt();
		}

		this.food = new Array<Actor>();
		for (var i:number=0; i<numFood; i++) {
			this.addFood();	
		}

		this.pheromones = new Pheromones(this.width, this.height);
		this.nest = new Nest(new Vec(this.width/2, this.height/2), 20);
	}	

	addAnt() {
		this.ants.push(new Ant(new Vec(this.width / 2, this.height / 2), this));
	}

	addFood() {
		this.food.push(new Food(new Vec(Math.random() * this.width, Math.random() * this.height), 1 + Math.floor(Math.random() * Food.MAX_SIZE - 1)));
	}

	update() {
		for (var i=0; i<this.ants.length; i++) {
			this.ants[i].update();
		}	
		this.updatePheromones();
	}	

	dropPheromone(pheromoneType:PheromoneType, pos:Vec, strength:number, spread:number) {
		var pheromones:Array<Array<number>> = this.pheromoneForType(pheromoneType);
		var x:number = Math.floor(pos.x)
		var y:number = Math.floor(pos.y)

		// Spread over larger area than a single point, decreasing in strength away from center.
		for(var i=x-spread; i<x+spread; i++) {
			var divisorX:number = Math.abs(i-x) + 1;
			for(var j=y-spread; j<y+spread; j++) {
				var divisorY:number = Math.abs(j-y) + 1;
				if (this.pointInside(i, j)) {
					pheromones[i][j] = Math.min(1, pheromones[i][j] + strength / (3 * divisorX * divisorY));
				}
			}
		}
	}

	getPheromoneAt(pos:Vec, pheromoneType:PheromoneType):number {
		if (! this.pointInside(pos.x, pos.y)) {
			return 0;
		}
		return this.pheromoneForType(pheromoneType)[Math.floor(pos.x)][Math.floor(pos.y)];
	}

	foodAtPos(pos:Vec):Food {
		for (var i=0; i<this.food.length; i++) {
			var food:Food = this.food[i];
			if (pos.insideCircle(food.pos.x, food.pos.y, food.size/2)) {
				return food;
			}
		}
		return null;	
	}

	private pheromoneForType(pheromoneType:PheromoneType):Array<Array<number>> {
		return pheromoneType == PheromoneType.Nest ? this.pheromones.nest : this.pheromones.food;
	}
	
	private pointInside(x:number, y:number): boolean {
		if (x < 0 || y < 0 || x >= this.width || y >= this.height) {
			return false;
		}
		return true;
	}
	
	private updatePheromones() {
		for (var x=0; x<this.width; ++x) {
			for (var y=0; y<this.height; ++y) {
				this.pheromones.nest[x][y] -= Ant.PHEROMONE_FADE;
				this.pheromones.nest[x][y] = Math.max(this.pheromones.nest[x][y], 0);
				this.pheromones.food[x][y] -= Ant.PHEROMONE_FADE;
				this.pheromones.food[x][y] = Math.max(this.pheromones.food[x][y], 0);
			}
		}
	 	
	}
}


