/// <reference path="Renderer.ts" />
/// <reference path="World.ts" />

(function(window, canvas){
	var renderer:Renderer = new Renderer(canvas);
	var world:World = new World(canvas.width, canvas.height, 3000, 300);

	var tick = function() {
		world.update();
		renderer.render(world.ants, world.food, world.nest, world.pheromones.nest, world.pheromones.food);
	}
	window.setInterval(tick, 1);
}(this, document.getElementById("simulation")));
